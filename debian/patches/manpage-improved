Author: Otto Forster
Description: Some improvements of manpage

Index: aribas/doc/aribas.1
===================================================================
--- aribas.orig/doc/aribas.1	2016-12-09 08:22:57.141609798 +0100
+++ aribas/doc/aribas.1	2016-12-09 08:27:19.490732144 +0100
@@ -2,7 +2,7 @@
 .\"
 .\" $Id: ssystem.1.2   1996/06/30 13:33:54 bousch Exp $
 .\"
-.TH ARIBAS 1 "February 2001" "ARIBAS"
+.TH ARIBAS 1 "February 2010" "ARIBAS"
 
 .SH NAME
 aribas \- Multiprecision Arithmetic Interpreter
@@ -11,7 +11,7 @@
 .B aribas
 [\fIoptions\fR] [<\fIari-file\fR> [<\fIarg1\fR> <\fIarg2\fR> ...]]
 
-This man page was written for Debian since the orginal software
+This man page was written for Debian since the original software
 did not contain a man page.
 
 .SH DESCRIPTION
@@ -37,13 +37,13 @@
 \fBAribas\fR answers
 .nf
 
-    -: 359907
+    \-: 359907
 
 .fi
-The symbol \fB-:\fR introduces the result.
+The symbol \fB\-:\fR introduces the result.
 .br
 .BR IMPORTANT.
-To mark the end of your input, you must always type a full stop `.' 
+To mark the end of your input, you must always type a full stop '.'
 and then press RETURN.
 .br
 .PP
@@ -51,7 +51,7 @@
 .nf
 
     ==> F6 := 2**64 + 1.
-    -: 18446_74407_37095_51617
+    \-: 18446_74407_37095_51617
  
 .fi
 This calculates the 6th Fermat number (\fB**\fR denotes exponentiation)
@@ -60,8 +60,8 @@
 this variable for example in the expression
 .nf
 
-    ==> 123**(F6 - 1) mod F6.
-    -: 688_66214_58712_63971 
+    ==> 123**(F6 \- 1) mod F6.
+    \-: 688_66214_58712_63971
 
 .fi
 which shows (by Fermat's theorem) that F6 is not a prime number.
@@ -72,7 +72,7 @@
 .nf
 
     ==> x := _.
-    -: 688_66214_58712_63971
+    \-: 688_66214_58712_63971
  
 .fi
 As you can see in the above examples, \fBaribas\fR uses the underscore
@@ -82,7 +82,7 @@
 .nf
 
     ==> z := 123_4567_890.
-    -: 1234567890
+    \-: 1234567890
  
 .fi
 Here the output contains no underscore, which shows that z is less
@@ -97,14 +97,14 @@
     working ..
     factor found after 512 iterations
 
-    -: 274177
+    \-: 274177
  
 .fi
 To find the remaining cofactor, give the command
 .nf
 
     ==> x := F6 div _.
-    -: 6728_04213_10721
+    \-: 6728_04213_10721
  
 .fi
 To test whether this factor is prime, Rabin's probabilistic test
@@ -112,7 +112,7 @@
 .nf
 
     ==> rab_primetest(x).
-    -: true
+    \-: true
  
 .fi
 The function \fIrho_factorize\fR is good for finding small factors
@@ -180,11 +180,11 @@
 
 .fi
 and several higher precisions up to an implementation dependent 
-limit, typically 1024 or 4096 bits, which can be determined by
+limit, typically 1024 or 5120 bits, which can be determined by
 the function \fImax_floatprec()\fR. By default, when
 calculating with numbers of data type \fIreal\fR, single_floats 
 are used. This corresponds to a precision of 9 to 10 decimal places.
-A precision of 4096 bits corresponds to over 1200 decimal places.
+A precision of 5120 bits corresponds to over 1500 decimal places.
 
 The precision can be changed using the function
 \fIset_floatprec\fR. The function takes one integer argument,
@@ -194,7 +194,7 @@
 .nf
 
     ==> set_floatprec(100).
-    -: 128
+    \-: 128
 
 .fi
 the floating point precision is 128 bits and you can calculate
@@ -227,7 +227,7 @@
 If you have entered this correctly, \fBaribas\fR echoes the function name
 .nf
 
-    -: fac
+    \-: fac
 
 .fi
 and from now on you can use \fIfac\fR in the same way as a built-in
@@ -235,7 +235,7 @@
 .nf
 
     ==> fac(32).
-    -: 2_63130_83693_36935_30167_21801_21600_00000
+    \-: 2_63130_83693_36935_30167_21801_21600_00000
  
 .fi
 Note that inside function definitions all used variables must
@@ -250,7 +250,7 @@
             i: integer;
         begin
             for i := 1 to n do
-                vec[i-1] := sqrt(i);
+                vec[i\-1] := sqrt(i);
             end;
             return vec;
         end.
@@ -261,7 +261,7 @@
 .nf
 
     ==> sqrt_list(10).
-    -: (1.00000000, 1.41421356, 1.73205081, 2.00000000, 
+    \-: (1.00000000, 1.41421356, 1.73205081, 2.00000000,
     2.23606798, 2.44948974, 2.64575131, 2.82842712, 3.00000000, 
     3.16227766)
 
@@ -298,7 +298,7 @@
 
 .fi
 returns a list of all keywords and names of builtin functions of \fBaribas\fR.
-This list has about 180 entries, and begins and ends as follows:
+This list has about 200 entries, and begins and ends as follows:
 .P
 (ARGV, _, __, ___, abs, alloc, and, arccos, arcsin, arctan, arctan2, aribas,
 array, atof, atoi, begin, binary, bit_and, bit_clear, 
@@ -341,7 +341,7 @@
 version of \fBaribas\fR, you sometimes get into the 
 following situation: Some previous line contains a typing error,
 but you cannot return to that line to correct it. 
-In this case you should simply type a full stop `\fB.\fR' , 
+In this case you should simply type a full stop '\fB.\fR' ,
 followed by RETURN. You will
 get an error message which you can safely ignore, and a new prompt
 \fB==>\fR appears, allowing you to try again.
@@ -354,50 +354,50 @@
 .SS options
 The following options are available:
 .TP
-.B -q
+.B \-q
 (quiet mode) Suppresses all messages to the screen (version no,
 copyright notice, etc.) when \fBaribas\fR is started
 .TP
-.B -v  
+.B \-v
 (verbose mode, default) Does not suppress messages to the screen when
 \fBaribas\fR is started.
 .TP
-.B -c <cols>
+.B \-c <cols>
 \fBaribas\fR does its own line breaking when writing to the screen.
 Normally it supposes that the screen (or the window in which \fBaribas\fR
-runs) has 80 columns. With the -c option you can set another number,
+runs) has 80 columns. With the \-c option you can set another number,
 which must be between 40 and 160 (in decimal representation).  For
 example, if you run \fBaribas\fR in an Xterm window with 72 columns, use the
-option -c72 (or -c 72, the space between -c and the number is
+option \-c72 (or \-c 72, the space between \-c and the number is
 optional).
 .TP
-.B -m <mem>
-Here <mem> is a number (in decimal representation) between 64 and
-16000. This number indicates how many Kilobytes of RAM \fBaribas\fR should
+.B \-m <mem>
+Here <mem> is a number (in decimal representation) between 500 and
+32000. This number indicates how many Kilobytes of RAM \fBaribas\fR should
 use for the \fBaribas\fR heap. The default value depends on the options used
 when \fBaribas\fR was compiled. Typically, under UNIX or LINUX it is 6
-Megabytes, corresponding to -m6000
+Megabytes, corresponding to \-m6000
 .TP
-.B -h <path of help file>
+.B \-h <path of help file>
 The online help of \fBaribas\fR depends on a file aribas.hlp which should be
 situated  in the range of the environment variable PATH. If this is not
-the case you can specify the exact path of the help file with the -h
+the case you can specify the exact path of the help file with the \-h
 option. If for example the file aribas.hlp is in the directory
-/usr/local/lib, use the option -h /usr/local/lib (the space after -h
-is not necessary).  The -h option can also be used if the help file
+/usr/local/lib, use the option \-h /usr/local/lib (the space after \-h
+is not necessary).  The \-h option can also be used if the help file
 has a different name. If the help file is named help-aribas and lies
-in the directory /home/joe/ari, use -h/home/joe/ari/help-aribas.
+in the directory /home/joe/ari, use \-h/home/joe/ari/help-aribas.
 
 With a properly installed Debian package of \fBaribas\fR it should
 not be necessary to use this option.
 .TP
-.B -p <ari-search-path>
+.B \-p <ari-search-path>
 With this option you can specify a search path for loading files with
 \fBaribas\fR source code. <ari-search-path> may be either the
 (absolute) pathname of one directory or several pathnames separated by
 colons.  Suppose that you have called \fBaribas\fR with the option
 
-	-p/usr/local/lib/aribas:~/ari/examples 
+	\-p/usr/local/lib/aribas:~/ari/examples 
 
 and that your home directory is /home/alice/. Then the command
 
@@ -408,7 +408,7 @@
 /home/alice/ari/examples.
 
 .TP
-.B -b
+.B \-b
 Batch mode when loading an \fBaribas\fR source code file from
 the command line, see below.
 
@@ -416,11 +416,11 @@
 One letter options which require no arguments may be merged,
 for example
 
-    aribas -q -b
+    aribas \-q \-b
 
 is equivalent to
 
-    aribas -qb
+    aribas \-qb
 
 .\"---------------------------------------------------------
 .SS Further command line arguments
@@ -432,8 +432,8 @@
 is loaded as if the command \fIload("<ari-file>")\fR had been given
 after the start of \fBaribas\fR at the \fBaribas\fR prompt. If the file is 
 not found in the current directory it is searched in the 
-directories specified by the -p option.
-If the option -b was given, the file is loaded and executed.
+directories specified by the \-p option.
+If the option \-b was given, the file is loaded and executed.
 Afterwards \fBaribas\fR exits without showing it's prompt. If
 the file cannot be loaded completely because of an error,
 \fBaribas\fR exits immediately after the error message.
@@ -451,7 +451,7 @@
 .nf
 
     ==> ARGV.
-    -: ("startup", "4536", "eisenstein")
+    \-: ("startup", "4536", "eisenstein")
 
 .fi
 If you need some arguments as numbers and not as strings, you can
@@ -460,7 +460,7 @@
 .nf
 
     ==> x := atoi(ARGV[1]).
-    -: 4536
+    \-: 4536
 
 .fi
 will do it. The length of the vector \fIARGV\fR can be determined by
@@ -528,27 +528,27 @@
 different from .arirc), including the full path.
 
 In the configuration file you can specify all command line options
-described above which begin with a - sign, however a separate
+described above which begin with a \- sign, however a separate
 line must be used for every single option. Lines beginning with
 the character # or empty lines are ignored.
 In addition to the options described above, the configuration
 file may contain \fBaribas\fR source code. For this purpose there
 must be a line reading
 
-.B -init
+.B \-init
 
 Then everything after this line is treated as \fBaribas\fR source code
 and executed when \fBaribas\fR is started.
 
 The existence of a configuration file for \fBaribas\fR does not exclude
 the possibility to give command line arguments. If an option
-(e.g. the -m option) is specified both in the configuration file
+(e.g. the \-m option) is specified both in the configuration file
 and the command line but with different values, then the 
-specification at the command line is valid. Analogously, a -v
-option on the command line overrides a -q option in the configuration
+specification at the command line is valid. Analogously, a \-v
+option on the command line overrides a \-q option in the configuration
 file.
-If there is -init code in the configuration file and an <ari-file>
-argument at the command line, then the -init code is executed first 
+If there is \-init code in the configuration file and an <ari-file>
+argument at the command line, then the \-init code is executed first 
 and afterwards the <ari-file> is loaded and its code executed.
 
 .SH FILES
@@ -569,7 +569,9 @@
 can be found in \fI/usr/share/doc/aribas\fR.
 .P
 The \fBaribas\fR home page is
-\fIhttp://www.mathematik.uni-muenchen.de/~forster/sw/aribas.html\fR.
+
+http://www.mathematik.uni-muenchen.de/~forster/sw/aribas.html
+
 .\"-------------------------------------------------------------
 .SH BUGS
 Bug reports should be sent by email to
@@ -581,4 +583,3 @@
 the aribas program. This man page was compiled by Ralf Treinen
 <treinen@debian.org> from the aribas documentation for the Debian
 package of aribas, and supplemented by the author.
-
